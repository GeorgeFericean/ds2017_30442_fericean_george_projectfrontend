import React, { Component } from 'react';
import './Navbar.css';
import {
    BrowserRouter as Router,
    Route,
    Link
} from 'react-router-dom';
import MainPage from "../main/MainPage";
class Navbar extends Component {
    constructor(props){
        super(props);
        this.state={
            hidden:true,
            role:"USER",
        }
    }
    componentDidMount(){
        this.setState({role:this.props.role});
    }
    renderNavbarForEmployee(){
        return(
            <div>
                <nav className="navbar navbar-inverse">
                    <div className="container-fluid">
                        <ul className="nav navbar-nav">
                            <li className="active"><Link to="/main">Home</Link></li>
                            <li className="active"><Link to="/add-product">Add Product</Link></li>
                            <li className="active"><Link to="/new-message">Send Message</Link></li>
                        </ul>
                        <ul className="nav navbar-nav navbar-right">
                            <li onClick={this.props.removeUser.bind(this)}><Link to="/"><span className="glyphicon glyphicon-log-in"></span> Log out</Link></li>
                        </ul>
                    </div>
                </nav>
            </div>
        )
    }
    renderNavbarForAdmin(){
        return(
            <div>
                <nav className="navbar navbar-inverse">
                    <div className="container-fluid">
                        <ul className="nav navbar-nav">
                            <li className="active"><Link to="/main">Home</Link></li>
                        </ul>
                        <ul className="nav navbar-nav navbar-right">
                            <li><Link to="/"><span className="glyphicon glyphicon-log-in"></span> Log out</Link></li>
                        </ul>
                    </div>
                </nav>
            </div>
        )
    }
    renderNavbarForUser(){
        return(
            <div>
                <nav className="navbar navbar-inverse">
                    <div className="container-fluid">
                        <ul className="nav navbar-nav">
                            <li className="active"><Link to="/main">Home</Link></li>
                            <li className="active"><Link to="/my-shopping-cart"><span className=" glyphicon glyphicon-shopping-cart"/> My Shopping Cart</Link></li>
                            <li className="active"><Link to="/my-favorites"><span className="glyphicon glyphicon-heart"/>  My Favourites</Link></li>
                            <li className="active"><Link to="/my-messages"> My Messages</Link></li>
                        </ul>
                        <ul className="nav navbar-nav navbar-right">
                            <li><Link to="/"><span className="glyphicon glyphicon-log-in"></span> Log out</Link></li>
                        </ul>
                    </div>
                </nav>
            </div>
        )
    }
    render() {
        if(this.props.role==="EMPLOYEE")
            return (
                <div>
                    {this.renderNavbarForEmployee()}
                </div>
            );
        if(this.props.role==="USER")
            return (
                <div>
                    {this.renderNavbarForUser()}
                </div>
            );
        if(this.props.role==="ADMIN")
            return (
                <div>
                    {this.renderNavbarForAdmin()}
                </div>
            );
    }
}

export default Navbar;
