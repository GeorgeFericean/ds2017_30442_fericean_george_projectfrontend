import React, { Component } from 'react';
import './Header.css';
import './logo-header.png'
class Header extends Component {
    constructor(props){
        super(props);
        this.state={
            hidden:true,
        }
    }
    render() {
        return (
         <div className="header-body">
             <img className="image-logo" src="http://maximizeonlineearning.com/wp-content/uploads/2016/11/12-Steps-to-Building-a-Successful-Ecommerce-Site-in-12-Months.png"/>
             <span>E-Commerce Website</span><span className="header-user">Hello, {this.props.loggedUser.username}</span>
         </div>

        );
    }
}

export default Header;
