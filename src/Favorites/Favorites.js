import React, { Component } from 'react';
import {Button, Panel} from "react-bootstrap";
import WishListAPI from "../API/WishListAPI";
class Favorites extends Component {
    constructor(props){
        super(props);
        this.state={
            wishlist:[],
        }
    }
    componentDidMount(){
        this.getWishlistForUser();
    }
    getWishlistForUser(){
        WishListAPI.getWishlist(this.props.loggedUser.id)
            .then((data) => {
                this.setState({wishlist:data});
            })
            .catch((error) => {
                console.error('Wishlist fetch failed');
            });
    }
    removeFromWishList(id){
        WishListAPI.deleteFromWishlist(this.props.loggedUser.id,id)
            .then((data) => {
            })
            .catch((error) => {
                console.error('Login failed');
            });
    }
    render() {
        return (
            <div className="shopping-cart-container">
                <h2 className="shopping-cart-title">My Favorites</h2>
                <div className="shopping-cart-items">
                    {this.state.wishlist.map((item)=>
                        <Panel className="shopping-cart-item">
                            {item.brand} {item.category} {item.type}
                            <div className="item-right">
                                <span>${item.price}</span>
                            </div>
                            <Button onClick={()=>this.removeFromWishList(item.id)}>Remove</Button>
                        </Panel>
                    )}

                </div>
            </div>
        );
    }
}

export default Favorites;
