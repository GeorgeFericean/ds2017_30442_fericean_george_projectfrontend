import React, { Component } from 'react';
import {Button, Col, ControlLabel, Form, FormControl, FormGroup} from "react-bootstrap";
import UsersAPI from "../API/UsersAPI";
import {Link} from "react-router-dom";
import  './Register.css'
class Register extends Component {
    constructor(props){
        super(props);
        this.state={
            username:"",
            fullName:"",
            Email:"",
            Address:"",
            Country:"",
            City:"",
            Telephone:"",
            IBAN:"",
            password:"",
            passwordConfirm:"",
            returnMessage:"",
        }
    }
    usernameHandler=(event)=>{
        this.setState({username:event.target.value});
    };
    fullnameHandler=(event)=>{
        this.setState({fullName:event.target.value});
    };
    emailHandler=(event)=>{
        this.setState({Email:event.target.value});
    };
    addressHandler=(event)=>{
        this.setState({Address:event.target.value});
    };
    countryHandler=(event)=>{
        this.setState({Country:event.target.value});
    };
    cityHandler=(event)=>{
        this.setState({City:event.target.value});
    };
    telephoneHandler=(event)=>{
        this.setState({Telephone:event.target.value});
    };
    ibanHandler=(event)=>{
        this.setState({IBAN:event.target.value});
    };
    passwordHandler=(event)=>{
        this.setState({password:event.target.value});
    };
    confirmPasswordHandler=(event)=>{
        this.setState({passwordConfirm:event.target.value});
    };


    register(){
        if(this.state.password!==this.state.passwordConfirm){
            this.setState({returnMessage:"The input passwords do not match"});
        }
        else {
            var obj = {};
            obj.username = this.state.username;
            obj.name = this.state.fullName;
            obj.email = this.state.Email;
            obj.address = this.state.Address;
            obj.country = this.state.Country;
            obj.city = this.state.City;
            obj.telephone = this.state.Telephone;
            obj.IBAN = this.state.IBAN;
            obj.password = this.state.password;
            var jsonString = JSON.stringify(obj);
            UsersAPI.createUser(jsonString)
                .then((data) => {
                    this.setState({returnMessage: data});
                })
                .catch((error) => {
                    console.error('Login failed');
                });
        }
    }

    render() {
        return (
            <div className="login-container">
                <h2 className="login-title">Sign Up</h2>

                <div className="login-form">
                    <div className="message-container"> <Col componentClass={ControlLabel} sm={12}>
                        {this.state.returnMessage}
                    </Col></div>
                    <Form horizontal >
                        <FormGroup controlId="form-horizontal-username">
                            <Col componentClass={ControlLabel} sm={2}>
                                Username
                            </Col>
                            <Col sm={6}>
                                <FormControl type="text" placeholder="Username" value={this.state.username} onChange={this.usernameHandler}/>
                            </Col>
                        </FormGroup>
                        <FormGroup controlId="form-horizontal-fullname">
                            <Col componentClass={ControlLabel} sm={2}>
                                Full Name
                            </Col>
                            <Col sm={6}>
                                <FormControl type="text" placeholder="Full Name" value={this.state.fullName} onChange={this.fullnameHandler}/>
                            </Col>
                        </FormGroup>
                        <FormGroup controlId="form-horizontal-email">
                            <Col componentClass={ControlLabel} sm={2}>
                                E-mail
                            </Col>
                            <Col sm={6}>
                                <FormControl type="e-mail" placeholder="E-mail" value={this.state.Email} onChange={this.emailHandler}/>
                            </Col>
                        </FormGroup>
                        <FormGroup controlId="form-horizontal-address">
                            <Col componentClass={ControlLabel} sm={2}>
                                Address
                            </Col>
                            <Col sm={6}>
                                <FormControl type="text" placeholder="Address" value={this.state.Address} onChange={this.addressHandler}/>
                            </Col>
                        </FormGroup>
                        <FormGroup controlId="form-horizontal-city">
                            <Col componentClass={ControlLabel} sm={2}>
                                City
                            </Col>
                            <Col sm={6}>
                                <FormControl type="text" placeholder="City" value={this.state.City} onChange={this.cityHandler}/>
                            </Col>
                        </FormGroup>
                        <FormGroup controlId="form-horizontal-country">
                            <Col componentClass={ControlLabel} sm={2}>
                                Country
                            </Col>
                            <Col sm={6}>
                                <FormControl type="text" placeholder="Country"  value={this.state.Country} onChange={this.countryHandler}/>
                            </Col>
                        </FormGroup>
                        <FormGroup controlId="form-horizontal-telephone">
                            <Col componentClass={ControlLabel} sm={2}>
                                Telephone
                            </Col>
                            <Col sm={6}>
                                <FormControl type="text" placeholder="Telephone" value={this.state.telephone} onChange={this.telephoneHandler}/>
                            </Col>
                        </FormGroup>
                        <FormGroup controlId="form-horizontal-IBAN">
                            <Col componentClass={ControlLabel} sm={2}>
                                IBAN
                            </Col>
                            <Col sm={6}>
                                <FormControl type="text" placeholder="IBAN" value={this.state.IBAN} onChange={this.ibanHandler}/>
                            </Col>
                        </FormGroup>
                        <FormGroup controlId="formHorizontalPassword">
                            <Col componentClass={ControlLabel} sm={2}>
                                Password
                            </Col>
                            <Col sm={6}>
                                <FormControl type="password" placeholder="Password"  value={this.state.password} onChange={this.passwordHandler} />
                            </Col>
                        </FormGroup>
                        <FormGroup controlId="formHorizontalPasswordConfirm">
                            <Col componentClass={ControlLabel} sm={2}>
                                Confirm Password
                            </Col>
                            <Col sm={6}>
                                <FormControl type="password" placeholder="Confirm Password" value={this.state.passwordConfirm} onChange={this.confirmPasswordHandler} />
                            </Col>
                        </FormGroup>

                        <FormGroup>
                            <Col smOffset={2} sm={10}>
                              <Button className="register-button" bsStyle="primary" bsSize="large" onClick={this.register.bind(this)}>
                                    Register
                                </Button>
                            </Col>
                        </FormGroup>
                        <FormGroup>
                            <Col smOffset={2} sm={10}>
                                <Link to="/"><Button className="register-button" bsStyle="primary" bsSize="large">
                                    Back to Login
                                </Button></Link>
                            </Col>
                        </FormGroup>
                    </Form>
                </div>
            </div>
        );
    }
}

export default Register;
