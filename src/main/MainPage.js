import React, { Component } from 'react';
import {Button, Col, Grid, Row, Table} from "react-bootstrap";
import './MainPage.css'
import ProductsAPI from "../API/ProductsAPI";
import {Link} from "react-router-dom";
import WishListAPI from "../API/WishListAPI";
import UsersAPI from "../API/UsersAPI";
class MainPage extends Component {
    constructor(props){
        super(props);
        this.state={
            loggedUser: this.props.loggedUser,
            role: "USER",
            users:[],
            products:[],
            categories:[],
            brands:[],
            showErrorMessage:false,
            errorMessage: "",
            brand:"",
            category:"",
            minPrice:null,
            maxPrice:null,
            searchString:"",
        }
    }
    componentWillMount(){

        this.getProducts();
        this.getAllBrands();
        this.getAllCategories();
        this.getAllUsers();
    }
    componentDidMount(){
        this.setState({role:this.props.role});
    }
    getProducts(){
        ProductsAPI.getProducts()
            .then((data) => {
                this.setState({products:data});
            })
            .catch((error) => {
                console.error('Login failed');
            });
    }
    clearSearch(){
        this.setState({searchString:""});
        this.getProducts();
    }
    clearSearchAdmin(){
        this.setState({searchString:""});
        this.getAllUsers();
    }
    getAllUsers(){
        UsersAPI.getUsers()
            .then((data) => {
                this.setState({users: data});
            })
            .catch((error) => {
                console.error('Login failed');
            });
    }
    deleteUser(id){
        let users=this.state.users;
        for(let i=0;i<users.length;i++) {
            if(users[i].id===id) {
                users.splice(i,1);
                break;
            }
        }
        this.setState({users:users});
        UsersAPI.deleteUser(id)
            .then((data) => {

            })
            .catch((error) => {
                console.error('Login failed');
            });
    }
    getAllCategories(){
        ProductsAPI.getCategories()
            .then((data) => {
                this.setState({categories:data});
            })
            .catch((error) => {
                console.error('Categories failed');
            });
    }
    getSearchResults(){
        ProductsAPI.searchProduct(this.state.searchString)
            .then((data) => {
                this.setState({products:data});
            })
            .catch((error) => {
                console.error('Login failed');
            });
    }
    getFilterResults(){
        console.log(this.state.brand+" "+ this.state.category+" "+this.state.minPrice+ " "+this.state.maxPrice );
        if(this.state.minPrice>this.state.maxPrice || this.state.minPrice===null || this.state.maxPrice===null || this.state.brand==="" || this.state.category==="") {
                this.setState({errorMessage:"The filters you entered are invalid"});
        }
        else
            ProductsAPI.filterProducts(this.state.brand, this.state.category, this.state.minPrice, this.state.maxPrice)
                .then((data) => {
                    this.setState({products:data});
                })
                .catch((error) => {
                    console.error('Login failed');
                });
    }
    getAllBrands(){
        ProductsAPI.getBrands()
            .then((data) => {
                this.setState({brands:data});
            })
            .catch((error) => {
                console.error('Brands failed');
            });
    }
    deleteProduct(id){
        let products=this.state.products;
        let productsDelete=this.state.products;
        for(let i=0;i<products.length;i++) {
            if(products[i].id===id) {
                productsDelete.splice(i,1);
                break;
            }
        }
        this.setState({products:productsDelete});
        ProductsAPI.deleteProduct(id)
            .then((data) => {

            })
            .catch((error) => {
                console.error('Login failed');
            });
    }
    handleSearchChange(event){
        this.setState({searchString: event.target.value});
    }
    handleBrandChange(event){
        this.setState({brand: event.target.value});
    }
    minPriceChange(event){
        this.setState({minPrice: event.target.value});
    }
    categoryChange(event){
        this.setState({category: event.target.value});
    }
    maxPriceChange(event){
        this.setState({maxPrice: event.target.value});
    }
    updateShoppingCart=(id)=>{
        let shoppingCart=this.props.shoppingCart;
        shoppingCart.push(id);
        this.props.updateShoppingCart(shoppingCart);
    };
    addToWishList(id){
        WishListAPI.addToWishList(this.props.loggedUser.id,id)
            .then((data) => {
            })
            .catch((error) => {
                console.error('Login failed');
            });
    };
    renderMainPageForEmployee(){
        return(

            <div className="table-container">
                <Table striped bordered condensed>
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Brand</th>
                        <th>Category</th>
                        <th>Type</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Color</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.state.products.map((item)=>(
                        <tr className="table-row">
                            <td>{item.id}</td>
                            <td>{item.brand}</td>
                            <td>{item.category}</td>
                            <td>{item.type}</td>
                            <td>{item.price}</td>
                            <td>{item.quantity}</td>
                            <td>{item.color}</td>
                            <td><Link to={"/edit-product/"+item.id} params={{ id: item.id}}><Button className="table-button"><span className="glyphicon glyphicon-pencil"></span></Button></Link></td>
                            <td><Button onClick={()=>this.deleteProduct(item.id)}  className="table-button"><span className="glyphicon glyphicon-trash"></span></Button></td>
                        </tr>
                    ))}
                    </tbody>
                </Table>
            </div>
        )
    }
    renderMainPageForAdmin(){
        return(
            <div className="table-container">
                <Table striped bordered condensed hover>
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Username</th>
                        <th>Full Name</th>
                        <th>E-mail</th>
                        <th>Address</th>
                        <th>City</th>
                        <th>Country</th>
                        <th>Telephone</th>
                        <th>IBAN</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.state.users.map((item)=>
                        <tr className="table-row">
                            <td>{item.id}</td>
                            <td>{item.username}</td>
                            <td>{item.name}</td>
                            <td>{item.email}</td>
                            <td>{item.address}</td>
                            <td>{item.city}</td>
                            <td>{item.country}</td>
                            <td>{item.telephone}</td>
                            <td>{item.iban}</td>
                            <td><Button className="table-button"><Link to={"/edit-user/"+item.id} params={{ id: item.id}}><span className="glyphicon glyphicon-pencil"></span></Link></Button></td>
                            <td><Button onClick={()=>this.deleteUser(item.id)}  className="table-button"><span className="glyphicon glyphicon-trash"></span></Button></td>
                        </tr>
                    )}

                    </tbody>
                </Table>
            </div>
        )
    }
    renderMainPageForUser(){
        return(
            <div className="pretty-products row">
                {this.state.products.map((item)=>(
                    <div className="pretty-product-container"><Link to={'/product-details/'+item.id} params={{ id: item.id}}>
                        <img src="http://makambaonline.com/wp-content/uploads/2017/07/10-questions-to-ask-when-buying-a-laptop.png"/>
                        <span>{item.brand} {item.category} {item.type} {item.color}</span>
                        <span>Product quantity: {item.quantity}</span>
                        <span>Product price: ${item.price}</span></Link>
                        <Button className="button-cart" onClick={()=>this.updateShoppingCart(item.id)}><span className=" glyphicon glyphicon-shopping-cart" />  Add To Shopping Cart</Button>
                        <Button className="button-favourites" onClick={()=>this.addToWishList(item.id)}><span className="glyphicon glyphicon-heart" />  Add to Favourites</Button>
                    </div>
                ))}
            </div>
        )
    }
    render() {
        if(this.props.role==="EMPLOYEE")
            return (
                <div>
                    <div className="search-toolbar">
                        <Grid>
                            <Row>
                                <Col xs={2} md={2} sm={2} lg={2}><span className="search-text">Search For a Product:</span></Col>
                                <Col xs={3} md={3} sm={3}><input className="search-input" type="text" placeholder="Search..." value={this.state.searchString} onChange={this.handleSearchChange.bind(this)}/></Col>
                                <Col xs={2} md={2} sm={2}> <Button bsStyle="primary" className="search-button" type="submit"  onClick={this.getSearchResults.bind(this)}>
                                    Search
                                </Button></Col>
                                <Col xs={2} md={2} sm={2}> <Button bsStyle="primary" className="search-button" type="submit" onClick={this.clearSearch.bind(this)}>
                                    Clear Filter
                                </Button></Col>
                            </Row>
                        </Grid>
                    </div>
                    {this.renderMainPageForEmployee()}
                </div>
            );
        if(this.props.role==="ADMIN")
            return (
                <div>
                    {this.renderMainPageForAdmin()}
                </div>
            );
        if(this.props.role==="USER")
            return (
                <div>
                    <div className="search-toolbar">
                        <Grid>
                            <Row>
                                <Col xs={2} md={2} sm={2} lg={2}><span className="search-text">Search For a Product:</span></Col>
                                <Col xs={3} md={3} sm={3}><input className="search-input" type="text" placeholder="Search..." value={this.state.searchString} onChange={this.handleSearchChange.bind(this)}/></Col>
                                <Col xs={2} md={2} sm={2}> <Button bsStyle="primary" className="search-button" type="submit"  onClick={this.getSearchResults.bind(this)}>
                                    Search
                                </Button></Col>
                                <Col xs={2} md={2} sm={2}> <Button bsStyle="primary" className="search-button" type="submit" onClick={this.clearSearch.bind(this)}>
                                    Clear Filter
                                </Button></Col>
                            </Row>
                            <Row className="filter-row">
                                <Col xs={1} md={1} sm={1} lg={1}><span className="search-text">Filter:</span></Col>
                                <Col xs={1} md={1} sm={1}><select className="select-input" value={this.state.brand} onChange={this.handleBrandChange.bind(this)}>
                                    <option>Select</option>
                                    {this.state.brands.map((brand)=>
                                        <option>{brand.name}</option>)}
                                </select></Col>
                                <Col xs={1} md={1} sm={1}><select className="select-input" value={this.state.category} onChange={this.categoryChange.bind(this)}>
                                    <option>Select</option>
                                    {this.state.categories.map((category)=>
                                        <option>{category.name}</option>)}
                                </select></Col>
                                <Col xs={3} md={3} sm={3} className="price-filter-container"><span>Minimum Price:</span><input className="price-input" type="number" placeholder="Minimum price" value={this.state.minPrice} onChange={this.minPriceChange.bind(this)}/></Col>
                                <Col xs={3} md={3} sm={3} className="price-filter-container"><span>Maximum Price:</span><input className="price-input" type="number" placeholder="Maximum price" value={this.state.maxPrice} onChange={this.maxPriceChange.bind(this)}/></Col>
                                <Col xs={1} md={1} sm={1}> <Button bsStyle="primary" className="search-button" onClick={this.getFilterResults.bind(this)}>
                                    Filter
                                </Button></Col>
                                <Col xs={1} md={1} sm={1}> <Button bsStyle="primary" className="search-button" onClick={this.clearSearch.bind(this)}>
                                    Clear Filter
                                </Button></Col>
                            </Row>
                        </Grid>
                    </div>
                    <div>{this.state.errorMessage}</div>
                    {this.renderMainPageForUser()}
                </div>
            );
    }
}

export default MainPage;
