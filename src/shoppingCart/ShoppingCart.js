import React, { Component } from 'react';
import './ShoppingCart.css';
import {Button, Panel} from "react-bootstrap";
import ProductsAPI from "../API/ProductsAPI";
var price=0;
class ShoppingCart extends Component {
    constructor(props){
        super(props);
        this.state={
            shoppingCart:[]
        }
    }
    componentWillMount(){
        this.getTotalPrice();
    }
    getTotalPrice(){
        let items=this.props.shoppingCart;
        for(let i=0;i<items.length;i++){
            price+=items[i].price;
            console.log(price);
        }
    }
    removeFromShoppingCart(){

    }
    render() {
        console.log(this.props.shoppingCart);
        return (
            <div className="shopping-cart-container">
                <h2 className="shopping-cart-title">My Shopping Cart</h2>
                <div className="shopping-cart-items">
                    {this.props.shoppingCart.map((item)=>
                        <Panel className="shopping-cart-item">
                            {item.brand} {item.category} {item.type}
                            <div className="item-right">
                            <span className="price-tag">Price: $ {item.price}</span>
                            </div>
                        </Panel>
                    )}
                    <div className="total-price">Total:$ {price}</div>
                    <div className="promo-code-container row"><span className="promo-code-text col-xs-3 col-sm-3 col-md-3">Have a promo code? Enter it here!</span>
                        <input type="text" className="col-xs-1 col-sm-1"/>
                        <Button className="promo-button">Enter</Button>
                    </div>
                    <Button bsStyle="success">
                        Checkout
                    </Button>

                </div>
            </div>
        );
    }
}

export default ShoppingCart;
