import React, { Component } from 'react';
import './MyMessages.css'
import {Button, Panel} from "react-bootstrap";
import MessageAPI from "../API/MessageAPI";
class MyMessages extends Component {
    constructor(props){
        super(props);
        this.state={
          messages:[],
            returnMessage:""
        }

    }
    componentDidMount(){
        this.getMessages();
    }
    deleteMessage(id){
        this.setState({returnMessage:""});
        let messages=this.state.messages;
        let productsDelete=this.state.messages;
        for(let i=0;i<messages.length;i++) {
            if(messages[i].id===id) {
                productsDelete.splice(i,1);
                break;
            }
        }
        this.setState({messages:productsDelete});
        MessageAPI.deleteMessage(id)
            .then((data) => {
                if(data.ok)
                this.setState({returnMessage:"Message deleted successfully"});
                else
                    this.setState({returnMessage:"Message deleting unsuccessful"});
            })
            .catch((error) => {
                console.error('Login failed');
            });
    }
    getMessages(){
        MessageAPI.getMessagesForUser(this.props.loggedUser.username)
            .then((data) => {
                this.setState({messages:data});
            })
            .catch((error) => {
                console.error('Login failed');
            });
    }
    render() {
        return (

            <div className="add-product-body">
                <div>{this.state.returnMessage}</div>
                <div>
                    {this.state.messages.map((item)=>
                        <Panel >
                            <span className="message-content">{item.content}</span>
                            <Button onClick={()=>this.deleteMessage(item.id)} className="message-button">Delete</Button>
                        </Panel>
                    )}

                </div>
            </div>

        );
    }
}

export default MyMessages;
