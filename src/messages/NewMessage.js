import React, { Component } from 'react';
import {Button, ButtonToolbar, ControlLabel, FormControl, FormGroup} from "react-bootstrap";
import UsersAPI from "../API/UsersAPI";
import PromoCodeAPI from "../API/PromoCodeAPI";
import MessageAPI from "../API/MessageAPI";
class NewMessage extends Component {
    constructor(props){
        super(props);
        this.state= {
            showMessage: true,
            showPromoCode: false,
            selectedUser: "",
            discount:0,
            users: [],
            content:"",
            responseMessage:""
        }
    }
    componentDidMount(){
        UsersAPI.getUsers()
        .then((data) => {
                this.setState({users: data});
            })
                .catch((error) => {
                    console.error('Login failed');
                });
    }
    handleMessageClick(){
        this.setState({responseMessage:""});
        this.setState({discount:0});
        this.setState({content:""});
        this.setState({selectedUser:""});
        this.setState({showMessage:true});
    }
    handlePromoClick(){
        this.setState({responseMessage:""});
        this.setState({discount:0});
        this.setState({content:""});
        this.setState({selectedUser:""});
        this.setState({showMessage:false});
    }
    handleSelectedUser(event){
        this.setState({responseMessage:""});
        this.setState({selectedUser:event.target.value});
    }
    handleDiscountChange(event){
        this.setState({responseMessage:""});
        this.setState({discount:event.target.value});
    }
    handleContentChange(event){
        this.setState({responseMessage:""});
        this.setState({content:event.target.value});
    }
    sendMessage(event){
        event.preventDefault();
        var obj = {};
        obj.content = this.state.content;
        obj.username = this.state.selectedUser;
        console.log(obj);
        var jsonString = JSON.stringify(obj);
        MessageAPI.sendMessage(jsonString)
            .then((data) => {
                this.setState({responseMessage:data});
            })
            .catch((error) => {

                console.error('Promo Code sending failed');
            });

    }
    sendPromoCode(event){
        event.preventDefault();
        var obj = {};
        obj.discount = this.state.discount;
        obj.username = this.state.selectedUser;
        var jsonString = JSON.stringify(obj);
        console.log(jsonString);
        PromoCodeAPI.createPromoCode(jsonString)
            .then((data) => {
                this.setState({responseMessage:data});
            })
            .catch((error) => {
                console.error('Message sending failed');
            });
    }
    render() {
        return (

            <div className="add-product-body">
                <div className="error-label">
                    <label>{this.state.responseMessage}</label>
                </div>
                <ButtonToolbar>
                    <Button bsStyle="primary" bsSize="large" onClick={this.handleMessageClick.bind(this)}>Message</Button>
                    <Button bsStyle="primary" bsSize="large" onClick={this.handlePromoClick.bind(this)}>Promo Code</Button>
                </ButtonToolbar>
                <div hidden={!this.state.showMessage}>
                <span className="page-text">New Message:</span>
                <div className="add-product-form">
                    <form>
                        <FormGroup controlId="formControlsSelect">
                            <ControlLabel>Username:</ControlLabel>
                            <FormControl componentClass="select" placeholder="select" value={this.state.selectedUser} onChange={this.handleSelectedUser.bind(this)}>
                                <option>Select</option>
                                {this.state.users.map((item)=>(
                                    <option value={item.username}>{item.username}</option>
                                ))}
                            </FormControl>
                        </FormGroup>
                        <FormGroup controlId="formControlsTextarea">
                            <ControlLabel>Message:</ControlLabel>
                            <FormControl componentClass="textarea" placeholder="textarea" value={this.state.content} onChange={this.handleContentChange.bind(this)} />
                        </FormGroup>


                        <Button type="submit" onClick={this.sendMessage.bind(this)}>
                            Submit
                        </Button>
                    </form>
                </div>
                </div>
                    <div hidden={this.state.showMessage}>
                        <span className="page-text">New Promo Code:</span>
                        <div className="add-product-form">
                            <form>
                                <FormGroup controlId="formControlsSelect">
                                    <ControlLabel>Username:</ControlLabel>
                                    <FormControl componentClass="select" placeholder="select" value={this.state.selectedUser} onChange={this.handleSelectedUser.bind(this)}>
                                        <option>Select</option>
                                        {this.state.users.map((item)=>(
                                            <option value={item.username}>{item.username}</option>
                                        ))}
                                    </FormControl>
                                </FormGroup>
                                <FormGroup controlId="formControlsDiscount">
                                    <ControlLabel>Discount:</ControlLabel>
                                    <FormControl type="number" value={this.state.discount} onChange={this.handleDiscountChange.bind(this)}/>
                                </FormGroup>
                                <Button type="submit" onClick={this.sendPromoCode.bind(this)}>
                                    Submit
                                </Button >
                            </form>
                        </div>
                    </div>

            </div>

        );
    }
}

export default NewMessage;
