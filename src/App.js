import React, { Component } from 'react';
import {
    BrowserRouter as Router,
    Route,
    Link, Switch
} from 'react-router-dom';
import LogIn from "./LogIn/LogIn";
import Register from "./Register/Register";
import MainPage from "./main/MainPage";
import Navbar from "./Navbar/Navbar";
import Header from "./header/Header";
import AddProduct from "./product/AddProduct";
import NewMessage from "./messages/NewMessage";
import ProductDetails from "./product/ProductDetails";
import ShoppingCart from "./shoppingCart/ShoppingCart";
import MyMessages from "./messages/MyMessages";
import Favorites from "./Favorites/Favorites";
import WishListAPI from "./API/WishListAPI";
import EditUser from "./users/EditUser";
import UsersAPI from "./API/UsersAPI";
import EditProduct from "./product/EditProduct";
import ProductsAPI from "./API/ProductsAPI";
var items=[];
class App extends Component {
    constructor(props){
        super(props);
        this.state= {
            hidden:true,
            loggedUser: {},
            role:"USER",
            shoppingCart: [],
            shoppingCartItems:[],
            WishList:[]
        }
    }
    updateShoppingCart(shoppingCart){
        this.setState({shoppingCart:shoppingCart});
        this.getItem(shoppingCart[shoppingCart.length-1]);
    }
    updateHideState(hidden){
        this.setState({hidden:hidden});
    }
    updateWishList(wishList){
        this.setState({WishList:wishList});
    }
    getUserRole(user){
        UsersAPI.getUserRole(user.username)
            .then((data) => {
               this.setState({role:data});
            })
            .catch((error) => {
                console.error('role failed');
            });
    }
    handleLogin(user){
        this.getUserRole(user);
        this.setState({loggedUser:user});
    }
    removeUser(){
        this.setState({loggedUser:{}});
    }
    getItem(id){
        ProductsAPI.getProductByID(id)
            .then((data) => {
                items.push(data);
            })
            .catch((error) => {
                console.error('Login failed');
            });
    }
  render() {
    console.log(this.state.role);
    return (
        <div>
            <div hidden={this.state.hidden}>
            <Header loggedUser={this.state.loggedUser}/>
            </div>
                <Router>
                <div>
                    <div hidden={this.state.hidden}>
            <Navbar loggedUser={this.state.loggedUser} role={this.state.role} removeUser={this.removeUser.bind(this)}/>
                    </div>
              <Switch>
              <Route exact path="/"
                     render={()=><LogIn
                         handleLogin={this.handleLogin.bind(this)}
                         updateHideState={this.updateHideState.bind(this)}/>}

                 />
              <Route exact path="/register" component={Register}/>
              <Route exact path="/main"
                     render={() => <MainPage
                     role={this.state.role}
                     shoppingCart={this.state.shoppingCart}
                     updateShoppingCart={this.updateShoppingCart.bind(this)}
                     loggedUser={this.state.loggedUser}
                     updateHideState={this.updateHideState.bind(this)}/>
                     }/>
                  <Route exact path="/add-product"
                         render={() =>
                         <AddProduct/>
                         }/>
                  <Route exact path="/edit-product/:id"
                         render={(...props) =>
                             <EditProduct
                                 {...props}
                             />
                         }/>
                  <Route exact path="/new-message"
                         render={() => <NewMessage
                         loggedUser={this.state.loggedUser}
                         />
                         }
                  />
                  <Route exact path="/product-details/:id"
                         render={(...props) => <ProductDetails
                         {...props}
                         shoppingCart={this.state.shoppingCart}
                         updateShoppingCart={this.updateShoppingCart.bind(this)}
                         loggedUser={this.state.loggedUser}
                         />}
                  />
                  <Route exact path="/edit-user/:id"
                         render={(...props) => <EditUser
                             {...props}
                             shoppingCart={this.state.shoppingCart}
                             updateShoppingCart={this.updateShoppingCart.bind(this)}
                             loggedUser={this.state.loggedUser}
                         />}
                  />
                  <Route exact path="/my-shopping-cart"
                         render={() => <ShoppingCart
                             shoppingCart={items}
                             updateShoppingCart={this.updateShoppingCart.bind(this)}
                             loggedUser={this.state.loggedUser}/>
                         }
                  />
                  <Route exact path="/my-favorites"
                         render={()=><Favorites
                             shoppingCart={items}
                             updateShoppingCart={this.updateShoppingCart.bind(this)}
                             loggedUser={this.state.loggedUser}
                             // WishList={this.state.WishList}
                             updateWishList={this.updateWishList.bind(this)}
                         />}

                  />
                  <Route exact path="/my-messages"
                         render={() =><MyMessages
                         loggedUser={this.state.loggedUser}/>}
                         />
              </Switch>
                </div>
          </Router>

        </div>
    );
  }
}

export default App;
