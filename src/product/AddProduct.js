import React, { Component } from 'react';
import {Button, ControlLabel, FormControl, FormGroup} from "react-bootstrap";
import './AddProduct.css'
import ProductsAPI from "../API/ProductsAPI";
import UsersAPI from "../API/UsersAPI";
class AddProduct extends Component {
    constructor(props){
        super(props);
        this.state= {
            brand: "",
            category: "",
            type: "",
            price: "",
            quantity: "",
            color: "",
            brands: [],
            categories: [],
            returnMessage:"",
        }

    }
    componentDidMount(){
        this.getAllBrands();
        this.getAllCategories();
    }
    getAllBrands(){
        ProductsAPI.getBrands()
            .then((data) => {
                this.setState({brands:data});
            })
            .catch((error) => {
                console.error('Brands fetching failed');
            });
    }
    getAllCategories(){
        ProductsAPI.getCategories()
            .then((data) => {
                this.setState({categories:data});
            })
            .catch((error) => {
                console.error('Categories fetching failed');
            });
    }
    handleBrandChange(event){
        this.setState({returnMessage: ""});
        this.setState({brand: event.target.value});
    }
    categoryChange(event){
        this.setState({returnMessage: ""});
        this.setState({category: event.target.value});
    }
    typeChange(event){
        this.setState({returnMessage: ""});
        this.setState({type: event.target.value});
    }
    priceChange(event){
        this.setState({returnMessage: ""});
        this.setState({price: event.target.value});
    }
    quantityChange(event){
        this.setState({returnMessage: ""});
        this.setState({quantity: event.target.value});
    }
    colorChange(event){
        this.setState({returnMessage: ""});
        this.setState({color: event.target.value});
    }
    saveProduct(event){
        event.preventDefault();
        var obj = {};
        obj.brand = this.state.brand;
        obj.category = this.state.category;
        obj.type = this.state.type;
        obj.price = this.state.price;
        obj.quantity = this.state.quantity;
        obj.color = this.state.color;
        var jsonString = JSON.stringify(obj);
        ProductsAPI.createProduct(jsonString)
            .then((data) => {
                this.setState({returnMessage: data});
            })
            .catch((error) => {
                console.error('Login failed');
            });
    }
    render() {
        console.log(this.props);
        return (

            <div className="add-product-body">
                <div><label className="label-message">{this.state.returnMessage}</label></div>
                <span className="page-text">Add a new Product:</span>
               <div className="add-product-form">
                <form>
                    <FormGroup controlId="formControlsSelect">
                        <ControlLabel>Brand</ControlLabel>
                        <FormControl componentClass="select" placeholder="select" value={this.state.brand} onChange={this.handleBrandChange.bind(this)}>
                            <option>Select</option>
                            {this.state.brands.map((brand)=>
                                <option>{brand.name}</option>)}
                        </FormControl>
                    </FormGroup>
                    <FormGroup controlId="formControlsSelect">
                        <ControlLabel>Category</ControlLabel>
                        <FormControl componentClass="select" placeholder="select" value={this.state.category} onChange={this.categoryChange.bind(this)}>
                            <option>Select</option>
                            {this.state.categories.map((brand)=>
                                <option>{brand.name}</option>)}
                        </FormControl>
                    </FormGroup>
                    <FormGroup>
                        <ControlLabel>Type</ControlLabel>
                        <FormControl type="text" placeholder="Type" value={this.state.type} onChange={this.typeChange.bind(this)} />
                    </FormGroup>
                    <FormGroup>
                        <ControlLabel>Price</ControlLabel>
                        <FormControl type="number" placeholder="Price" value={this.state.price} onChange={this.priceChange.bind(this)}/>
                    </FormGroup>
                    <FormGroup>
                        <ControlLabel>Quantity</ControlLabel>
                        <FormControl type="number" value={this.state.quantity} onChange={this.quantityChange.bind(this)}/>
                    </FormGroup>
                    <FormGroup>
                        <ControlLabel>Color</ControlLabel>
                        <FormControl type="text" placeholder="Color" value={this.state.color} onChange={this.colorChange.bind(this)} />
                    </FormGroup>

                    <Button type="submit" onClick={this.saveProduct.bind(this)}>
                        Submit
                    </Button>
                </form>
               </div>
            </div>

        );
    }
}

export default AddProduct;
