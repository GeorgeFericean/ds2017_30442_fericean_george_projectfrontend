import React, { Component } from 'react';
import './ProductDetails.css'
import {Button} from "react-bootstrap";
import WishListAPI from "../API/WishListAPI";
import ProductsAPI from "../API/ProductsAPI";
class ProductDetails extends Component {
    constructor(props){
        super(props);
        this.state={
            favoritesButtonText:"",
            id:this.props[0].match.params.id,
            product:[]
        }
    }
    componentDidMount(){
        ProductsAPI.getProductByID(this.props[0].match.params.id)
            .then((data) => {
            this.setState({product:data});
            })
            .catch((error) => {
                console.error('Product get failed');
            });
    }
    updateShoppingCart(id){
        let shoppingCart=this.props.shoppingCart;
        shoppingCart.add(id);
        this.props.updateShoppingCart(shoppingCart);
    }
    addToWishList(id){
        WishListAPI.addToWishList(this.props.loggedUser.id,id)
            .then((data) => {
        })
            .catch((error) => {
                console.error('Login failed');
            });
    }
    removeFromWishList(id){
        WishListAPI.deleteFromWishlist(this.props.loggedUser.id,id)
            .then((data) => {
            })
            .catch((error) => {
                console.error('Login failed');
            });
    }
    render() {
        return (
            <div>
                    <div className="product-details-body">
                    <span className="product-title">{this.state.product.brand} {this.state.product.category} {this.state.product.type}</span>
                    <div className="row product-stats">
                        <div className="image-container col-lg-6 col-sm-6 col-xs-6 col-md-6">
                        <img className="product-img" src="http://makambaonline.com/wp-content/uploads/2017/07/10-questions-to-ask-when-buying-a-laptop.png"/>
                        </div>
                        <div className="stats-container col-lg-3 col-sm-3 col-xs-3 col-md-3">
                        <span className="product-text-header">Brand:<span className="product-text">{this.state.product.brand}</span></span>
                    <span className="product-text-header">Category:<span className="product-text"> {this.state.product.category}</span></span>
                    <span className="product-text-header">Type:<span className="product-text"> {this.state.product.type}</span></span>
                    <span className="product-text-header">Color:<span className="product-text"> {this.state.product.color}</span></span>
                    <span className="product-text-header">Number left in stock:<span className="product-text"> {this.state.product.quantity}</span></span>
                    </div>
                    <div className="operations-container col-lg-3 col-sm-3 col-xs-3 col-md-3">
                        <span className="product-text-header">Price:<span className="product-text">$ {this.state.product.price}</span></span>
                    <Button className="button-cart"><span className=" glyphicon glyphicon-shopping-cart" onClick={()=>this.props.updateShoppingCart(this.state.product.id)}/>  Add To Shopping Cart</Button>
                    <Button className="button-favourites"><span className="glyphicon glyphicon-heart" onClick={()=>this.addToWishList(this.state.product.id)}/>  Add to Favourites</Button>
                    </div>
                    </div>
                    </div>
                )}
            </div>
        );
    }
}

export default ProductDetails;
