class PromoCodeAPI {
    static createPromoCode(data) {
        return fetch(
            `http://localhost:8080/spring-demo/promo/create`, {
                headers: {
                    'Content-Type':  'application/json',
                },
                method: 'POST',
                body:data,
            }).then((response) => {
            if(response.ok){
                return response.json();
            }
        })
            .catch((error)=>{
                console.error("Login API error");
                throw error;
            })
    }
    static deletePromoCode() {
        return fetch(
            `http://localhost:8080/spring-demo/promo/delete/{code}`, {
                headers: {
                    'Content-Type':  'application/json',
                },
                method: 'GET',
            }).then((response) => {
            if(response.ok){
                return response.json();
            }
        })
            .catch((error)=>{
                console.error("Login API error");
                throw error;
            })
    }
    static getPromoCode() {
        return fetch(
            `http://localhost:8080/spring-demo/promo/get/{code}`, {
                headers: {
                    'Content-Type':  'application/json',
                },
                method: 'GET',
            }).then((response) => {
            if(response.ok){
                return response.json();
            }
        })
            .catch((error)=>{
                console.error("Login API error");
                throw error;
            })
    }
}
export default PromoCodeAPI;