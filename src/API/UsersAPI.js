class UsersAPI {
    static getUsers() {
        return fetch(
            `http://localhost:8080/spring-demo/users/all`, {
                headers: {
                    'Content-Type':  'application/json',
                },
                method: 'GET',
            }).then((response) => {
            if(response.ok){
                 return response.json();
            }
        })
            .catch((error)=>{
                console.error("Login API error");
                throw error;
            })
    }
    static updateUser(user, id) {
        return fetch(
            `http://localhost:8080/spring-demo/users/update/${id}`, {
                headers: {
                    'Content-Type':  'application/json',
                },
                method: 'POST',
                body:user,
            }).then((response) => {
            if(response.ok){
                return response.text();
            }
        })
            .catch((error)=>{
                console.error("Login API error");
                throw error;
            })
    }
    static deleteUser(id) {
        return fetch(
            `http://localhost:8080/spring-demo/users/delete/${id}`, {
                headers: {
                    'Content-Type':  'application/json',
                },
                method: 'DELETE',
            }).then((response) => {
            if(response.ok){
                return response.text();
            }
        })
            .catch((error)=>{
                console.error("Login API error");
                throw error;
            })
    }
    static createUser(formData) {
        return fetch(
            `http://localhost:8080/spring-demo/users/create`, {
                headers: {
                    'Content-Type':  'application/json',
                },
                method: 'POST',
                body:formData
            }).then((response) => {
            if(response.ok){
                return response.text();
            }
        })
            .catch((error)=>{
                console.error("Login API error");
                throw error;
            })
    }
    static getUserByUsername(username) {
        return fetch(
            `http://localhost:8080/spring-demo/users/getByUsername/${username}`, {
                headers: {
                    'Content-Type':  'application/json',
                },
                method: 'GET',
            }).then((response) => {
            if(response.ok){
                return response.json();
            }
        })
            .catch((error)=>{
                console.error("User by username API error");
                throw error;
            })
    }
    static getUserByName(name) {
        return fetch(
            `http://localhost:8080/spring-demo/users/getByName/${name}`, {
                headers: {
                    'Content-Type':  'application/json',
                },
                method: 'GET',
            }).then((response) => {
            if(response.ok){
                return response.json();
            }
        })
            .catch((error)=>{
                console.error("Login API error");
                throw error;
            })
    }
    static getUserByID(id) {
        return fetch(
            `http://localhost:8080/spring-demo/users/getByID/${id}`, {
                headers: {
                    'Content-Type':  'application/json',
                },
                method: 'GET',
            }).then((response) => {
            if(response.ok){
                return response.json();
            }
        })
            .catch((error)=>{
                console.error("Login API error");
                throw error;
            })
    }
    static getUserRole(username) {
        return fetch(
            `http://localhost:8080/spring-demo/user/role/${username}`, {
                headers: {
                    'Content-Type':  'application/json',
                },
                method: 'GET',
            }).then((response) => {
                console.log(response);
                return response.text();

        })
            .catch((error)=>{
                console.error("Login API error");
                throw error;
            })
    }



}
export default UsersAPI;