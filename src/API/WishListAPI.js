class WishListAPI {
    static getWishlist(userId) {
        return fetch(
            `http://localhost:8080/spring-demo/wishlist/get/${userId}`, {
                headers: {
                    'Content-Type':  'application/json',
                },
                method: 'GET',
            }).then((response) => {
            if(response.ok){
                return response.json();
            }
        })
            .catch((error)=>{
                console.error("WishList API error");
                throw error;
            })
    }
    static addToWishList(userId,prodId) {
        return fetch(
            `http://localhost:8080/spring-demo/wishlist/add/${userId}/${prodId}`, {
                headers: {
                    'Content-Type':  'application/json',
                },
                method: 'POST',
            }).then((response) => {
            if(response.ok){
                return response.json();
            }
        })
            .catch((error)=>{
                console.error("Login API error");
                throw error;
            })
    }
    static deleteFromWishlist(userId, prodId) {
        return fetch(
            `http://localhost:8080/spring-demo/wishlist/delete/${userId}/${prodId}`, {
                headers: {
                    'Content-Type':  'application/json',
                },
                method: 'GET',
            }).then((response) => {
            if(response.ok){
                return response.json();
            }
        })
            .catch((error)=>{
                console.error("Login API error");
                throw error;
            })
    }
}
export default WishListAPI;