class MessageAPI {
    static sendMessage(data) {
        return fetch(
            `http://localhost:8080/spring-demo/message/send`, {
                headers: {
                    'Content-Type':  'application/json',
                },
                method: 'POST',
                body:data,
            }).then((response) => {
            if(response.ok){
                return response.json();
            }
            else return response.statusText;
        })
            .catch((error)=>{
                console.error("Send message error");
                throw error;
            })
    }
    static getMessagesForUser(username) {
        return fetch(
            `http://localhost:8080/spring-demo/message/all/${username}`, {
                headers: {
                    'Content-Type':  'application/json',
                },
                method: 'GET',
            }).then((response) => {
            if(response.ok){
                return response.json();
            }
        })
            .catch((error)=>{
                console.error("Login API error");
                throw error;
            })
    }
    static deleteMessage(id) {
        return fetch(
            `http://localhost:8080/spring-demo/message/delete/${id}`, {
                headers: {
                    'Content-Type':  'application/json',
                },
                method: 'GET',
            }).then((response) => {
                return response;

        })
            .catch((error)=>{
                console.error("Login API error");
                throw error;
            })
    }
}
export default MessageAPI;