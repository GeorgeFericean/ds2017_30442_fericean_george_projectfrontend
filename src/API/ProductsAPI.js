class ProductsAPI {
    static getProducts() {
        return fetch(
            `http://localhost:8080/spring-demo/products/all`, {
                headers: {
                    'Content-Type':  'application/json',
                },
                method: 'GET'
            }).then((response) => {
            if(response.ok){

                return response.json();
            }
            // else
            //   return response.statusText;
        })
            .catch((error)=>{
                console.error("Login API error");
                throw error;
            })
    }
    static getBrands() {
        return fetch(
            `http://localhost:8080/spring-demo/brands/all`, {
                headers: {
                    'Content-Type':  'application/json',
                },
                method: 'GET'
            }).then((response) => {
            if(response.ok){

                return response.json();
            }
            // else
            //   return response.statusText;
        })
            .catch((error)=>{
                console.error("Login API error");
                throw error;
            })
    }
    static getCategories() {
        return fetch(
            `http://localhost:8080/spring-demo/categories/all`, {
                headers: {
                    'Content-Type':  'application/json',
                },
                method: 'GET'
            }).then((response) => {
            if(response.ok){

                return response.json();
            }
            // else
            //   return response.statusText;
        })
            .catch((error)=>{
                console.error("Login API error");
                throw error;
            })
    }
    static createProduct(form) {
        return fetch(
            `http://localhost:8080/spring-demo/employee/product/create`, {
                headers: {
                    'Content-Type':  'application/json',
                },
                method: 'POST',
                body:form
            }).then((response) => {
            if(response.ok){
                return response.json();
            }
            // else
            //   return response.statusText;
        })
            .catch((error)=>{
                console.error("Login API error");
                throw error;
            })
    }
    static getProductByID(id) {
        return fetch(
            `http://localhost:8080/spring-demo/employee/product/get/${id}`, {
                headers: {
                    'Content-Type':  'application/json',
                },
                method: 'GET'
            }).then((response) => {
            if(response.ok){
                return response.json();
            }
        })
            .catch((error)=>{
                console.error("Login API error");
                throw error;
            })
    }
    static updateProduct(product,id) {
        return fetch(
            `http://localhost:8080/spring-demo/employee/product/update/${id}`, {
                headers: {
                    'Content-Type':  'application/json',
                },
                method: 'POST',
                body:product,
            }).then((response) => {
            if(response.ok){

                return response.json();
            }
        })
            .catch((error)=>{
                console.error("Login API error");
                throw error;
            })
    }
    static deleteProduct(id) {
        return fetch(
            `http://localhost:8080/spring-demo/employee/product/delete/${id}`, {
                headers: {
                    'Content-Type':  'application/json',
                },
                method: 'GET'
            }).then((response) => {
            if(response.ok){

                return response.json();
            }
        })
            .catch((error)=>{
                console.error("Login API error");
                throw error;
            })
    }
    static searchProduct(searchString) {
        return fetch(
            `http://localhost:8080/spring-demo/search/${searchString}`,{
                headers: {
                    'Content-Type':  'application/json',
                },
                method: 'GET'
            }).then((response) => {
            if(response.ok){

                return response.json();
            }
            // else
            //   return response.statusText;
        })
            .catch((error)=>{
                console.error("Login API error");
                throw error;
            })
    }
    static filterProducts(brand,category,priceMin,priceMax) {
        return fetch(
            `http://localhost:8080/spring-demo/user/filter/${brand}/${category}/${priceMin}/${priceMax}`,{
        headers: {
            'Content-Type':  'application/json',
        },
        method: 'GET'
    }).then((response) => {
            if(response.ok){

                return response.json();
            }
            // else
            //   return response.statusText;
        })
            .catch((error)=>{
                console.error("Login API error");
                throw error;
            })
    }



}
export default ProductsAPI;