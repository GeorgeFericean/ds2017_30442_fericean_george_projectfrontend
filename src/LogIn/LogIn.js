import React, { Component } from 'react';
import './LogIn.css'
import UserLogInAPI from "../API/UserLogInAPI";
import {Button, Col, ControlLabel, Form, FormControl, FormGroup} from "react-bootstrap";
import {Link, withRouter} from "react-router-dom";
import UsersAPI from "../API/UsersAPI";
class LogIn extends Component {
    constructor(props){
        super(props);
        this.state={
                username:"",
                password:"",
                role: "",
                user: {}

        }
    }
    componentDidMount(){
        this.props.updateHideState(true);
    }
    userHandler=(event)=>{
        this.setState({username:event.target.value});
    }
    passHandler=(event)=>{
        this.setState({password:event.target.value});
    }
    login=(event)=> {
        const username = this.state.username;
        const password = this.state.password;

        // UserLogInAPI.logIn(username,password)
        //     .then((data) => {
        //         this.setState({user:data});
        //     })
        //     .catch((error) => {
        //         console.error('Login failed');
        //     });
        UsersAPI.getUserByUsername(username)
            .then((data) => {
                this.setState({user: data});
                this.props.updateHideState(false);
                this.props.handleLogin(data);

            })
            .catch((error) => {
                console.error('Username fetch failed');
            });

    };
    render() {
        return (
            <div className="login-container">
                    <h2 className="login-title">Login to your account</h2>
                <div className="login-form">
                    <Form horizontal >
                        <FormGroup controlId="form-horizontal-username">
                            <Col componentClass={ControlLabel} sm={2}>
                                Username
                            </Col>
                            <Col sm={6}>
                                <FormControl type="text" placeholder="Username" onChange={this.userHandler}/>
                            </Col>
                        </FormGroup>

                        <FormGroup controlId="formHorizontalPassword">
                            <Col componentClass={ControlLabel} sm={2}>
                                Password
                            </Col>
                            <Col sm={6}>
                                <FormControl type="password" placeholder="Password" onChange={this.passHandler} />
                            </Col>
                        </FormGroup>

                        <FormGroup>
                            <Col smOffset={2} sm={10}>
                                 <Link to="/main"><Button className="login-button" bsStyle="primary" bsSize="large" onClick={this.login}>
                                    Sign in
                                 </Button></Link>
                                <Link to="/register"><Button className="register-button" bsStyle="primary" bsSize="large" >
                                    Register
                                </Button> </Link>
                            </Col>
                        </FormGroup>
                    </Form>
                </div>
            </div>
        );
    }
}

export default withRouter(LogIn);
